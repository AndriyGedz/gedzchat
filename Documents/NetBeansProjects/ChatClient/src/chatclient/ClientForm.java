package chatclient;

import static config.ProgectConfig.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.swing.*;
import messages.Dispatcher;
import messages.IMessage;
import messages.NewMsg;

/**
 * 
 * @author Гедз
 */
public class ClientForm extends javax.swing.JFrame {

    /**
     * Creates new form ClientForm
     */
    public ClientForm() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        logInDialog = new javax.swing.JDialog();
        btnLogIn = new javax.swing.JButton();
        textLogIn = new javax.swing.JTextField();
        label = new javax.swing.JLabel();
        lblServerIP = new javax.swing.JLabel();
        tFServerIp = new javax.swing.JTextField();
        panel = new javax.swing.JPanel();
        send = new javax.swing.JButton();
        message = new javax.swing.JTextField();
        javax.swing.JScrollPane clientListScrollPane = new javax.swing.JScrollPane();
        clientList = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();

        logInDialog.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        logInDialog.setTitle("LogIn");
        logInDialog.setMinimumSize(new java.awt.Dimension(400, 200));
        logInDialog.setModal(true);
        logInDialog.setUndecorated(false);
        logInDialog.setResizable(false);

        btnLogIn.setText("LogIn");
        btnLogIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogInActionPerformed(evt);
            }
        });

        label.setText("Enter your login and start messaging");

        lblServerIP.setText("Server Ip");

        tFServerIp.setText("localhost");

        javax.swing.GroupLayout logInDialogLayout = new javax.swing.GroupLayout(logInDialog.getContentPane());
        logInDialog.getContentPane().setLayout(logInDialogLayout);
        logInDialogLayout.setHorizontalGroup(
            logInDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(logInDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(logInDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(label)
                    .addComponent(textLogIn, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
                    .addComponent(lblServerIP)
                    .addComponent(tFServerIp))
                .addGap(18, 18, 18)
                .addComponent(btnLogIn)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        logInDialogLayout.setVerticalGroup(
            logInDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, logInDialogLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(label)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textLogIn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblServerIP)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(logInDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tFServerIp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLogIn))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        label.getAccessibleContext().setAccessibleName("");
        label.getAccessibleContext().setAccessibleDescription("");
        lblServerIP.getAccessibleContext().setAccessibleName("lblServerIp");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ChatClient");
        setMinimumSize(new java.awt.Dimension(800, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                logIn(evt);
            }
        });

        panel.setLayout(new java.awt.GridBagLayout());

        send.setText("Send");
        send.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendMessage(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        panel.add(send, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.8;
        gridBagConstraints.weighty = 0.1;
        panel.add(message, gridBagConstraints);

        clientList.setModel(clientModel);
        clientList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        clientListScrollPane.setViewportView(clientList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.4;
        panel.add(clientListScrollPane, gridBagConstraints);

        jList1.setModel(messageModel);
        jScrollPane2.setViewportView(jList1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.6;
        gridBagConstraints.weighty = 0.6;
        panel.add(jScrollPane2, gridBagConstraints);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    /**
     * Send message to the server.
     * 
     */     
    private void sendMessage(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendMessage
        try {
            clientConnection.sendMessage(
                    new NewMsg(IMessage.NEW_MSG, message.getText()));
        } catch (Exception ex) {
            throw new ChatException(
                    "Can not send message. " + 
                    (ex.getMessage() != null?ex.getMessage(): ""));
        }
    }//GEN-LAST:event_sendMessage
    /*
     * show logIn dialog
     */
    private void logIn(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_logIn
        logInDialog.setLocationRelativeTo(null);
        logInDialog.setVisible(true);
    }//GEN-LAST:event_logIn
    /*
     * action when main window closed
     */
    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        clientConnection.interrupt();
    }//GEN-LAST:event_formWindowClosed
    /*
     * action when you try connect to server
     */
    private void btnLogInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogInActionPerformed
        if (clientConnection == null) {
            try {
                if(textLogIn.getText().length() < 4){
                    throw new ChatException("Length of login must be more then 4"); 
                }
                clientConnection = new ClientConnection(
                        new Socket(InetAddress.getByName(tFServerIp.getText()), PORT),
                        new ClientDispatcher());
                clientConnection.start();
                clientConnection.sendMessage(
                        new NewMsg(IMessage.NEW_CLIENT, textLogIn.getText()));
            } catch (UnknownHostException e) {
                throw new ChatException("Don't know about host : " + 
                        tFServerIp.getText());
            } catch (IOException e) {
                throw new ChatException("Couldn't get I/O for the connection");
            }
        }
    }//GEN-LAST:event_btnLogInActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws IOException {
        final ClientForm frame = new ClientForm();
        Thread.setDefaultUncaughtExceptionHandler(frame.new ExceptionHandler());
        System.setProperty("sun.awt.exception.handler",
                ExceptionHandler.class.getName());
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                frame.setVisible(true);
            }
        });


    }
    /*
     * store messages
     */
    private DefaultListModel<String> messageModel = new DefaultListModel();
    /*
     * store clients
     */
    private DefaultListModel<String> clientModel = new DefaultListModel<>();
    ClientConnection clientConnection;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogIn;
    private javax.swing.JList clientList;
    private javax.swing.JList jList1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel label;
    private javax.swing.JLabel lblServerIP;
    private javax.swing.JDialog logInDialog;
    private javax.swing.JTextField message;
    private javax.swing.JPanel panel;
    private javax.swing.JButton send;
    private javax.swing.JTextField tFServerIp;
    private javax.swing.JTextField textLogIn;
    // End of variables declaration//GEN-END:variables

    private class ClientDispatcher implements Dispatcher {

        @Override
        public void addMessage(String aMessage) {
            messageModel.addElement(aMessage);
        }

        @Override
        public void addClient(String nickname) {
            clientModel.addElement(nickname);
        }

        @Override
        public void deleteClient(String nickname) {
            clientModel.removeElement(nickname);
        }

        @Override
        public void logOk() {
            logInDialog.setVisible(true);
            logInDialog.dispose();
        }

        @Override
        public void logErr() {
            clientConnection.closeConnection();
            clientConnection = null;
            JOptionPane.showMessageDialog(logInDialog, "login used!");
        }
    }

    public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

        public void handle(Throwable thrown) {
            // for EDT exceptions
            handleException(Thread.currentThread().getName(), thrown);
        }

        @Override
        public void uncaughtException(Thread thread, Throwable thrown) {
            // for other uncaught exceptions
            handleException(thread.getName(), thrown);
        }

        protected void handleException(String tname, Throwable thrown) {
            JOptionPane.showMessageDialog(panel, thrown.getMessage());
        }
    }
}
