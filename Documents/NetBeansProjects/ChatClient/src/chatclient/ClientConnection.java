/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chatclient;

import messages.LogErrHandler;
import messages.LogOkHandler;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import messages.Dispatcher;
import messages.HandlerList;
import messages.IMessage;
import server.ClientInfo;
import messages.AddClientHandler;
import messages.NewMsgHandler;
import messages.RemoveClientHandler;

/**
 *
 * @author Гедз
 */
class ClientConnection extends Thread {

    private Socket socket;
    private ObjectInputStream mIn;
    private ObjectOutputStream mOut;
    private HandlerList handlerList = new HandlerList();

    public ClientConnection(Socket mSocket, Dispatcher disp) throws IOException {

        this.socket = mSocket;
        mOut = new ObjectOutputStream(socket.getOutputStream());
        mIn = new ObjectInputStream(socket.getInputStream());
        handlerList.addHandler(
                new AddClientHandler(disp));
        handlerList.addHandler(
                new RemoveClientHandler(disp));
        handlerList.addHandler(
                new NewMsgHandler(disp));
        handlerList.addHandler(
                new LogErrHandler(disp));
        handlerList.addHandler(
                new LogOkHandler(disp));
    }

    /**
     * Until interrupted, reads messages from the client socket, forwards them
     * to the server dispatcher's queue and notifies the server dispatcher.
     */
    @Override
    public void run() {
        IMessage message;
        try {
            while (!isInterrupted()) {
                message = (IMessage) mIn.readObject();
                if (message == null) {
                    break;
                }
                disptchMessage(message);
            }
        } catch (IOException ioex) {
            throw new ChatException(ioex.getMessage());
        } catch (ClassNotFoundException ex) {
            throw new ChatException(ex.getMessage());
        } finally {
            closeConnection();
        }
    }

    public void sendMessage(IMessage message) throws IOException {
        mOut.writeObject(message);
        mOut.flush();
    }

    private void disptchMessage(IMessage message) {
        handlerList.handleMessage(message);
    }

    public void closeConnection() {
        this.interrupt();
        try {
            if (mIn != null) {
                mIn.close();
            }
            if (mOut != null) {
                mOut.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException ex) {
        }
    }
}