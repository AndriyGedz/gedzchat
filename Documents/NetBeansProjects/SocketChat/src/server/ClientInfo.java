/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

/**
 *
 * @author Гедз
 */
import java.net.Socket;
 
/**
 *
 * @author Гедз
 */
public class ClientInfo
{
    /*
     * store socket for each client
     */
    private final Socket mSocket;
    /*
     * store client listener thread 
     */
    private ClientListener mClientListener;
    /*
     * store client stender thread
     */
    private ClientSender mClientSender;
    /*
     * store client nickname
     */
    private String nickname;
    
    ClientInfo(Socket socket) {
        this.mSocket = socket;
    }
    
    /**
     * @return the mClientListener
     */
    public ClientListener getClientListener() {
        return mClientListener;
    }

    /**
     * @return the mClientSender
     */
    public ClientSender getClientSender() {
        return mClientSender;
    }

    /**
     * @return the nickname
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * @return the mSocket
     */
    public Socket getSocket() {
        return mSocket;
    }

    /**
     * @param nickname the nickname to set
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * @param mClientListener the mClientListener to set
     */
    public void setClientListener(ClientListener mClientListener) {
        this.mClientListener = mClientListener;
    }

    /**
     * @param mClientSender the mClientSender to set
     */
    public void setClientSender(ClientSender mClientSender) {
        this.mClientSender = mClientSender;
    }
    
    
}
