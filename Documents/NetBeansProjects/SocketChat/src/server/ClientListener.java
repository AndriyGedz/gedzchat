package server;

/**
 *
 * @author Гедз
 */
import java.io.*;
import java.net.*;
import messages.*;
import messages.AddClientHandler;
import messages.NewMsgHandler;
import messages.RemoveClientHandler;
 
public class ClientListener extends Thread implements Dispatcher
{
    private ServerDispatcher mServerDispatcher;
    private ClientInfo mClientInfo;
    private ObjectInputStream mIn;
    
    private HandlerList handlerList = new HandlerList();
    {
        handlerList.addHandler(
                new AddClientHandler(this));
        handlerList.addHandler(
                new RemoveClientHandler(this));
        handlerList.addHandler(
                new NewMsgHandler(this));
    }
    public ClientListener(ClientInfo aClientInfo, ServerDispatcher aServerDispatcher)
    throws IOException
    {
        mClientInfo = aClientInfo;
        mServerDispatcher = aServerDispatcher;
        Socket socket = aClientInfo.getSocket();
        mIn = new ObjectInputStream(socket.getInputStream());
        
    }
    
    @Override
    public void addClient(String nickname){
        mServerDispatcher.addClient(nickname, mClientInfo);
    }
    
    @Override
    public void addMessage(String message){
        mServerDispatcher.sendMessageToAllClients(
                new NewMsg(
                    IMessage.NEW_MSG, 
                    mClientInfo.getNickname() + " : " + message));
    }
    /**
     * Until interrupted, reads messages from the client socket, forwards them
     * to the server dispatcher's queue and notifies the server dispatcher.
     */
    @Override
    public void run()
    {
        IMessage message;
        try {
           while (!isInterrupted()) {
               message = (IMessage) mIn.readObject();
               if (message == null)
                   break;
               mServerDispatcher.dispatchMessage(message, handlerList);
           }
        } catch (IOException ioex) {
            System.err.println(ioex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
        } finally {
            if (mIn != null) {
                try {
                    mIn.close();
                    mServerDispatcher.deleteClient(mClientInfo.getNickname());
                } catch (IOException ex) {
                     System.err.println("Last data was not saved: " +
                            ex.getMessage());
                }
            }
            mClientInfo.getClientSender().interrupt();
            
        }
    }

    @Override
    public void deleteClient(String nickname) {
        this.interrupt();
    }

    @Override
    public void logOk() {
        throw new UnsupportedOperationException("Not supported for server."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void logErr() {
        throw new UnsupportedOperationException("Not supported for server."); //To change body of generated methods, choose Tools | Templates.
    }
}
