/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

/**
 *
 * @author Гедз
 */
import java.io.IOException;
import java.util.Map.Entry;
import java.util.concurrent.*;
import messages.*;
public class ServerDispatcher {
    /*
     * store information about all connected clients
     */
    private ConcurrentMap<String, ClientInfo> mClients = new ConcurrentHashMap();

    /**
     * execute arrived message with mHanler dispatchMessage method is called by other threads
     * (ClientListener) when a message is arrived.
     */
    public synchronized void dispatchMessage(
            IMessage message, HandlerList handlerList) {
        handlerList.handleMessage(message);
    }

    /**
     * Sends given message to all clients in the client list. Actually the
     * message is added to the client sender thread's message queue and this
     * client sender thread is notified.
     */
    public synchronized void sendMessageToAllClients(
            IMessage aMessage) {
        for (Entry<String, ClientInfo> client : mClients.entrySet()) {
            client.getValue().getClientSender().sendMessage(aMessage);
        }
    }    
    
    /*
     * add client to client list
     */
    public void addClient(String nickName, ClientInfo mClientInfo) {
        if (!mClients.containsKey(nickName)) {
            mClients.putIfAbsent(nickName, mClientInfo);
            mClientInfo.getClientSender().sendMessage(
                    new NewMsg(IMessage.LOG_OK, nickName));
            notifyClientAdded(nickName, mClientInfo);
            mClientInfo.setNickname(nickName);
        } else {
            mClientInfo.getClientSender().sendMessage(
                    new NewMsg(IMessage.LOG_ERR, nickName));
        }
    }
    /*
     * delete client from client list
     */
    public void deleteClient(String nickname) throws IOException {
        mClients.get(nickname).getSocket().close();
        mClients.remove(nickname);
        sendMessageToAllClients(new NewMsg(IMessage.DEL_ClIENT, nickname));
    }
    /*
     * close all connections in client list
     */
    void closeAllConnections() {
        for (Entry<String, ClientInfo> client : mClients.entrySet()) {
            client.getValue().getClientListener().interrupt();
            client.getValue().getClientSender().interrupt();
        }
    }

    private void notifyClientAdded(String nickName, ClientInfo mClientInfo) {
        for (Entry<String, ClientInfo> client : mClients.entrySet()) {
            if (!client.getKey().equals(nickName)) {
                mClientInfo.getClientSender().sendMessage(
                        new NewMsg(IMessage.NEW_CLIENT, client.getKey()));
            }
        }
        sendMessageToAllClients(new NewMsg(IMessage.NEW_CLIENT, nickName));
    }
    
}
