/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;


import java.io.*;
import java.net.*;
import java.util.concurrent.*;
import messages.*;
 /**
 * Send message to client
 * 
 * @author Гедз
 */
public class ClientSender extends Thread
{
    /*
     * Store message queue
     */
    private BlockingQueue<IMessage> mMessageQueue = new ArrayBlockingQueue(10);
    /*
     * store information about client
     */
    private ClientInfo clientInfo;
    /*
     * output stream in which writes messages
     */
    private ObjectOutputStream out;
    /*
     * constructor that init field clientInfo and open output stream
     */
    public ClientSender(ClientInfo clientInfo)
            throws IOException {
        this.clientInfo = clientInfo;
        Socket socket = clientInfo.getSocket();
        out = new ObjectOutputStream(socket.getOutputStream());
    }

    /**
     * Adds given message to the message queue sendMessage 
     */
    public void sendMessage(IMessage aMessage) {
        mMessageQueue.add(aMessage);
    }

    /**
     * Sends given message to the client's socket.
     */
    private void sendMessageToClient(IMessage aMessage) throws IOException {
        out.writeObject(aMessage);
        out.flush();
    }

    /**
     * Until interrupted, reads messages from the message queue and sends them
     * to the client's socket.
     */
    @Override
    public void run() {
        try {
            while (!isInterrupted()) {
                try {
                    IMessage message = mMessageQueue.take();
                    sendMessageToClient(message);
                } catch (InterruptedException e) {
                    System.err.println(e.getMessage());                    
                } catch (IOException ioe) {
                    System.err.println("Message was not send: " + 
                            ioe.getMessage());
                }
            }
        } finally {
            // Communication is broken. Interrupt both listener and sender threads
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ex) {
                    System.err.println("Last data was not saved: " +
                            ex.getMessage());
                }
            }
            clientInfo.getClientListener().interrupt();
        }
    }
}