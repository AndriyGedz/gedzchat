package server;

import java.net.*;
import java.io.*;
import static config.ProgectConfig.*;

/**
 * The main server class. It open server socket for lstening and start listeing
 * port. <br>
 * For each client connection creates listener and sender wich communicates with
 * ServerDispatcher
 *
 * @author Гедз
 */
public class Server {

    public static void main(String[] args) {
        // Open server socket for listening
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(PORT);
            System.out.println("ChatServer started on port " + PORT);
        } catch (IOException se) {
            System.err.println("Can not start listening on port " + PORT);
            System.exit(-1);
        }
        acceptClients(serverSocket);

    }

    private static void acceptClients(ServerSocket serverSocket) {
        ServerDispatcher serverDispatcher = new ServerDispatcher();
        Socket socket;
        // Accept and handle client connections
        try {
            while (true) {
                socket = serverSocket.accept();
                ClientInfo clientInfo = new ClientInfo(socket);
                ClientSender clientSender =
                        new ClientSender(clientInfo);
                ClientListener clientListener =
                        new ClientListener(clientInfo, serverDispatcher);
                clientInfo.setClientListener(clientListener);
                clientInfo.setClientSender(clientSender);
                clientListener.start();
                clientSender.start();
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            if (serverSocket != null) {
                try {
                    serverDispatcher.closeAllConnections();
                    serverSocket.close();
                } catch (IOException ex) {
                }
            }
        }
    }
}
