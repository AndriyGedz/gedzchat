/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

/**
 *
 * @author Гедз
 */
public class RemoveClientHandler extends AbstractHandler{
    
    public RemoveClientHandler(Dispatcher client){
        super(client);
    }

    @Override
    public boolean handleInput(IMessage message) {
        if (message.getType().equals(IMessage.DEL_ClIENT)) {
            dispatcher.deleteClient(message.getMessage());
            return true;
        }
        return false;
    }
}