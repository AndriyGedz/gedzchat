/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

/**
 *
 * @author Гедз
 */
public class LogErrHandler extends AbstractHandler {
    
    public LogErrHandler(Dispatcher dispatcher) {
        super(dispatcher);
    }

    @Override
    public boolean handleInput(IMessage message) {
        if (message.getType().equals(IMessage.LOG_ERR)) {
            dispatcher.logErr();
            return true;
        }
        return false;
    }
}
