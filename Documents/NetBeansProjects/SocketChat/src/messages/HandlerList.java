/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

import java.util.*;

/**
 *
 * @author Гедз
 */
public class HandlerList {
    private List<MessageHandler> handlerList = new ArrayList<>();
    
    public void addHandler(MessageHandler handler){
        handlerList.add(handler);
    }
    
    public void handleMessage(IMessage mesage){
        for(MessageHandler handler : handlerList){
            if(handler.handleInput(mesage)){
                break;
            }
        }
    }
}
