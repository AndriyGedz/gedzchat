/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

/**
 *
 * @author Гедз
 */
public class NewMsgHandler extends AbstractHandler{
    
    public NewMsgHandler(Dispatcher client){
        super(client);
    }

    @Override
    public boolean handleInput(IMessage message) {
        if (message.getType().equals(IMessage.NEW_MSG)) {
            dispatcher.addMessage(message.getMessage());
            return true;
        }
        return false;
    }
    
}
