/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;


/**
 * abstract handler for message handler
 * @author Гедз
 */
public abstract class AbstractHandler implements MessageHandler{
    /*
     * store dispatcher witch execute messages
     */
    protected Dispatcher dispatcher;
    
    /*
     * costructor witch set dispatcher
     * 
     * @author
     */
    public AbstractHandler(Dispatcher dispatcher){
        this.dispatcher = dispatcher;
    } 
}
