/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

/**
 *
 * @author Гедз
 */
public class LogOkHandler extends AbstractHandler {
    
    public LogOkHandler(Dispatcher dispatcher) {
        super(dispatcher);
    }

    @Override
    public boolean handleInput(IMessage message) {
        if (message.getType().equals(IMessage.LOG_OK)) {
            dispatcher.logOk();
            return true;
        }
        return false;
    }
}
