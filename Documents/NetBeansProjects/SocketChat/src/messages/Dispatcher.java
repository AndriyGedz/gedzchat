/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

/**
 *
 * @author Гедз
 */
public interface Dispatcher {
    public void addMessage(String aMessage);
    public void addClient(String nickname);
    public void deleteClient(String nickname);
    public void logOk();
    public void logErr();
}
