package messages;

import java.io.Serializable;

/**
 * 
 * @author Гедз
 */
public interface IMessage extends Serializable{
    
    public static final String NEW_MSG = "message";
    public static final String NEW_CLIENT = "add_client";
    public static final String DEL_ClIENT = "del_client";
    public static final String LOG_OK = "log_ok";
    public static final String LOG_ERR = "log_err";
    
    String getType();
    String getMessage();
}