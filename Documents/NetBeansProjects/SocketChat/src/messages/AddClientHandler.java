/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package messages;

/**
 * handler for add client message
 * @author Гедз
 */
public class AddClientHandler extends AbstractHandler{
    /*
     * overrided constructor
     */
    public AddClientHandler(Dispatcher dispatcher){
        super(dispatcher);
    }

    @Override
    public boolean handleInput(IMessage message) {
        if (message.getType().equals(IMessage.NEW_CLIENT)) {
            dispatcher.addClient(message.getMessage());
            return true;
        }
        return false;
    }
}
